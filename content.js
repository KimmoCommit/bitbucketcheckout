var bbBaseUrl = "bitbucket.org";
var splittedCurUrl = window.location.href.split('/');
var locationBitbucket = splittedCurUrl[2] === bbBaseUrl;
var isReviewingPR = splittedCurUrl[5] === 'pull-requests';
var branchNameEl = document.querySelector('dd.branch > a');

if(branchNameEl){

  var branchName = branchNameEl.getAttribute('title');

  //GETTING PARENT EL WHERE TO APPEND CHILDREN
  var el = document.querySelector("#pull-request-header");
  var container = document.createElement('div');
  container.setAttribute('style', 'width:100%;padding:10px');
  // BUTTON EL
  var btn = document.createElement('button');
  btn.setAttribute('class','aui-button');
  btn.setAttribute('style', 'margin-right: 10px');
  btn.innerHTML = 'Checkout';

  // CHECBOX LABEL EL
  var checkBoxLabel = document.createElement('label');
  checkBoxLabel.innerHTML = 'Run tests';

  // CHECKBOX EL
  var checkBox = document.createElement('input');
  checkBox.setAttribute('type', 'checkbox');
  checkBox.setAttribute('id', 'bbCheckout-checkbox');
  checkBox.setAttribute('checked', 'checked');


  // CMD
  var command = 'git fetch --all && git checkout ' + branchName + ' && ' + 'git pull origin ' + branchName + ':' + branchName;

  // SPAN EL
  var spanEl = document.createElement('span');
  spanEl.setAttribute('style', 'position:absolute;left:-9999px');

  // APPENDING
  checkBoxLabel.appendChild(checkBox);
  document.getElementsByTagName('body')[0].appendChild(spanEl);

  // COPYING TO CLIPBOARD
  var selection = window.getSelection();
  var range = document.createRange();
  btn.addEventListener('click', function(e) {
    var actuallyCmd = checkBox.checked ? command + ' && npm run test' : command
    spanEl.innerHTML = actuallyCmd;
    range.selectNodeContents(spanEl);
    selection.removeAllRanges();
    selection.addRange(range);
    var successful = document.execCommand('copy');
    if(successful){
      console.log('copied \n' + actuallyCmd)
    } else {
      console.log('copied failed');
    }
    window.getSelection().removeAllRanges()
  });
container.appendChild(btn);
container.appendChild(checkBoxLabel);
el.appendChild(container);
}
